<img src="/uploads/5820008300b1078b3afb0a12d91269ee/ezgif-1-afcd104269f4.gif" alt="enter the slash command" width="100%"/>

This project provides all the neccessary code to host a slack bot which can be used to conduct story poker sessions for GitLab tickets directly in Slack and automate workflows that stem from the decisions of the session. If you find this project helpful, please remember to leave a star!
___
> **Note: if you are setting up a serverless deployment, create a file under /src/ and include the following code to get access to the express application**
```
import { generateApp } from './Server/index'
import App from './Server/app';

const app = new App(generateApp()).app;
```
# Quick Navigation
*  [Inspiration](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#inspiration)
*  [Features](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#features)
*  [Work flow](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#work-flow)
*  [Technologies used](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#technologies-used)
*  [Setup](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#setup)
   *  [Slack bot creation](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#slack-bot-creation)
   *  [Configuration file](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#configuration-file)
      *  [Slack credentials](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#slack-credentials)
      *  [GitLab credentials](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#gitlab-credentials)
      *  [Users to tag for grooming](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#users-to-tag-for-grooming)
      *  [Labels to remove or add](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#labels-to-removeadd)
      *  [Project name and id](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#project-name-and-id)
      *  [Text for gitlab note](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#text-for-gitlab-note)
      *  [Initial message](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#initial-message)
      *  [Port](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#port)
   *  [Linking your bot to your express server](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#linking-your-bot-to-your-express-server)
*  [Local execution](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#local-execution)

___

# Inspiration

For about a year, my team used a popular slack app called `storypokerbot`.  I decided to build this tool as an alternative dedicated to GitLab workflows to further automate the process. 

### [you can find this story poker bot here](https://storypokerbot.herokuapp.com/ )

___

# Features

* See all ticket information in Slack
* Minimal clutter left in slack channel
* Automated tagging of specified users when needed
* Automated label changes for your unique workflow
* Weight finalization directly from Slack
* Documents slack discussion directly in the ticket for ease of record keeping 

___

# Work flow

1. Tag your bot in the channel you want to use it in so that you can add the bot to the channel.
2. Grab the url of a [ticket](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot/-/issues/3)
3. Enter your specified slash command followed by the ticket url 

   <img src="https://i.imgur.com/wfi9rY5.png" alt="enter the slash command" width="600"/>

4. Wait until you are satisified with the number of participants who have voted on a weight 

   <img src="https://i.imgur.com/uyvHdHu.png" alt="some voters have weighed" width="700"/> 

5. Click Reveal 

   <img src="https://i.imgur.com/Dd7u97T.png" alt="votes have been revealed" width="700"/>

6. Have whatever discussion is needed to reach a consensus 

   <img src="https://i.imgur.com/Gf2M1NT.png" alt="comments were added" width="700"/>

7. Select a decided weight from the dropdown and click finalize 

   <img src="https://i.imgur.com/umPVTBy.png" alt="weight was finalized" width="700"/>

8. Verify that the [ticket](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot/-/issues/3) has been updated with:
   * The Selected Weight
   * A note linking back to the slack thread
   * Added labels specified in your configuration file
   * Removed labels specified in your configuration file
___

# Technologies used

* Slack API
* GitLab API
* Express
* Node JS
* Typescript

# Setup
## Slack bot creation
1. Navigate to the [Slack API site](https://api.slack.com/apps) and click the button that says `Create New App` 

   <img src="https://i.imgur.com/oPETZxX.png" alt="App Creation" width="700"/>

2. Enter a name for your app, and then select you teams workspace.  Then click the button that says `Create App` 
   
   <img src="https://i.imgur.com/mDx3Bgo.png" alt="App details screen" width="400"/>

3. Click on the left menu item that says `OAuth & Permissions` 

   <img src="https://i.imgur.com/KBi2oj3.png" alt="Basic information screen" width="700"/>

4. Add the following scopes:
   * `channels:history`
   * `chat:write`
   * `commands`
   * `files:write`
   * `groups:history`
   * `im:history`
   * `mpim:history`

5. Click the button that says `Install App to Workspace` 

   <img src="https://i.imgur.com/TJUdUhG.png" alt="Oauth and Permissions screen" width="700"/>

6. On the next screen, click `Allow` 

   <img src="https://i.imgur.com/o6R6RV2.png" alt="Approval for Install" width="700"/>

## Configuration file
###  Slack credentials
   *  slackApiToken
      1. Navigate to your bots `OAuth & Permissions` screen
      2. Grab the value in the box labeled `Bot User OAuth Access Token` 
      
         <img src="https://i.imgur.com/Pvzvw8n.png" alt="OAuth token" width="680"/>

> **NOTE:  This should be secured and kept secret.  Do not share this value and it is suggested that you do not put it into your repository.  The code is setup to allow you to store the value as an environment variable.**

   *  slackSigningSecret
      1.  Navigate to your bots `Basic Information` screen 
      
            <img src="https://i.imgur.com/dK9X9jv.png" alt="Basic information menu item" width="680"/>

      2.  In the section titled `App Credentials`, find the section that says `Signing Secret` 
      
            <img src="https://i.imgur.com/sELKt9D.png" alt="Signing Secret" width="680"/>

      3.  Click `Show` to get your signing secret
> **NOTE:  This should be secured and kept secret.  Do not share this value and it is suggested that you do not put it into your repository.  The code is setup to allow you to store the value as an environment variable.**
###  GitLab credentials
The user whose credentials you use here will be the user who performs the actions in GitLab that result from the actions taken using this bot.
   *  Navigate to GitLab
   *  Click on your icon in the top right corner of the page 
   
        <img src="https://i.imgur.com/SuzSajO.png" alt="Profile drop down" width="700"/>

   *  Click `Settings` from the resulting dropdown 

        <img src="https://i.imgur.com/Z8CWiBz.png" alt="Settings Menu link" width="500"/>

   *  On the left, there will be a menu with an icon that looks like an oval with three dots 
   in the middle.  It will say `Access Tokens`.  Click it. 
   
        <img src="https://i.imgur.com/3wLOndK.png" alt="Access token menu link" width="500"/>
   *  Enter a name for your token, as well as a date if you want.  Make sure you select `api` for scopes, and then click the button that says `Create Personal Access Token`. 
   
        <img src="https://i.imgur.com/906Jd6I.png" alt="enter credentials" width="700"/>
   *  When the page reloads, your access token will be present.   Make sure you copy it immediately because you will not be able to retrieve this again. 
   
        <img src="https://i.imgur.com/OerO5Ha.png" alt="Onetime access token" width="700"/>

> **NOTE:  This should be secured and kept secret.  Do not share this value and it is suggested that you do not put it into your repository.  The code is setup to allow you to store the value as an environment variable.**
###  Users to tag for grooming
   1. Click on a users image in slack 
   
       <img src="https://i.imgur.com/ZXQTqQh.png" alt="slack user profile picture" width="400"/>
   
   2. When their user information loads, click the link that says `View full profile` 
   
       <img src="https://i.imgur.com/XKWWLSO.png" alt="slack user profile" width="400"/>
   
   3. Click the button that says `More` and the click `Copy Member ID` 
   
       <img src="https://i.imgur.com/fryiNOo.png" alt="Getting member id" width="400"/>

   4. Repeat this process for every user you wish to involve in the process
   5. Add every Id to the array in the configuration file

###  Labels to remove/add
   *  This field is not required
   *  This is unique to your teams needs.  Whenever a weight is finalized, labels you specify here will be removed or added if available.  
   *  If you add a label to `labelsToRemove` and it is not on the issue when you finalize a weight, nothing will break.
   *  Example: If you want the label `ready-for-prioritization` to be added after you finalize a weight, simply add `'ready-for-prioritization'` to the member `labelsToAdd`.
   
###  Project name and id
   *  This field is not required
   *  If you navigate to a project, you should see the ID and the name next to the icon for that project.  
   
       <img src="https://i.imgur.com/DjJmmxm.png" alt="Project Id" width="400"/>

   *  For this project, you would simply add the following:

          {name: 'Gitlab-Slack-StoryPokerBot', id: '19486626'}
   *  When you weigh a ticket, the bot will parse the url to grab the project name.  It will then check the configuration file to see if there is a specified project already listed with that project name.  
> **NOTE:  This field is actually optional.  If you try to a weigh a ticket and the project is not listed here, it will simply do a search using GitLabs Api in order to get the project information needed to complete the task.  Be aware that the GitLab user who's token is being used in the configuration file MUST be a member of that project in order for it to be returned**

###  Text for GitLab note
   *  When you finalize a weight, the bot will add a note to the issue that links back to thread in Slack where the weighing took place
   *  The value in this configuration item will be used for the text of that link.
###  Initial message
   *  This is what will pop up in Slacks Toast notification.  When you are notified about a message in slack, usually you will see a message that contains some text in the bottom right corner of your monitor.   That is a Toast notification.
###  Port
   *  The port of your server you want your bot to listen to.

## Linking your bot to your express server
1. However you choose to deploy the express app, you will need access to the base url where your express server can be reached.
2. There are two relevent endpoints that need to be available and provided for the slack bot.
   1. The first is for the initial slash command and it can be found at `gitlab-slack-storypokerbot/src/Server/Routes/WeigherRoutes.ts` on line 18 (it is `/gitlab/weigh` by default)
   2. The second is for slack actions and it can be found at `gitlab-slack-storypokerbot/src/Server/app.ts` on line 30 (it is `/slack/actions` by default)
3. Navigate to your slack bot settings on the [slack website](https://api.slack.com) and click on `Slash Commands` on the left side menu 
    
    <img src="https://i.imgur.com/aKzqzdh.png" alt="Slash commands" width="700"/>

4. Click the button that says `Create New Command`
5. Enter the command you want to use when initiating the process.  We use `/weigh` in our team. (**NOTE: slash commands are globally scoped so whatever you enter must be unique**)
6. In the request url, enter your base url, followed by the endpoint specified in `WeigherRoutes.ts` For Example:
          
          https://example.com/gitlab/weigh

7. Enter a short description as prompted and a usage hint like `[url]`
8. Leave `Escape channels, users, and links sent to your app` unchecked
9. Click the green `Save` button 

    <img src="https://i.imgur.com/pU1IBby.png" alt="Slash Command configuration" width="700"/>

10. Next, click the left hand menu item `Interactivity & Shortcuts` 

    <img src="https://i.imgur.com/7YZGa4f.png" alt="Interactivity and shortcuts" width="700"/>

11. Flip the Toggle for `Interactivity` to `On`
12. In the box with the header `Interactivity`, enter the endpoint from `app.ts`. It should look something like:

        https://example.com/slack/actions
13. Click `Save Changes` 

    <img src="https://i.imgur.com/Yz7wF3Z.png" alt="Actions url" width="700"/>

And you should be good to go!

___
# Local execution

1. Ensure you have a bot that is set up according to the steps defined in the [Slack Bot Creation section](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#slack-bot-creation)
2. clone the repository to your machine
3. Setup your configuration file according to the [instructions listed above.](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#configuration-file)  
4. Ensure the port specified in your configuration file is expost
    * [I use Ngrok to expose my ports for things like this.](https://medium.com/automationmaster/how-to-use-ngrok-to-forward-my-local-port-to-public-5e9b148ff31c)
5. Run `npm install --dev` to get your dependencies.
6. Run `npm run dev` to begin the server.
7. Grab the url that directs to your server (if you used ngrok, it should be listed in the terminal as mentioned in the article).
8. Follow the steps listed in [Linking your bot to your express server](https://gitlab.com/JordanUpperman/gitlab-slack-storypokerbot#configuration-file)

Your bot should now be working!

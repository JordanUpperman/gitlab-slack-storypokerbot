import { IGenericSlackPayload } from 'Server/ThirdPartyWrappers/Slack/IGenericSlackPayload';

const getDefaults = (): IGenericSlackPayload => ({
  message: {
    text: "someText",
    ts: "messageTs",
    blocks: [],
    thread_ts: "messageThreadTs"
  }, 
  channel: {
    id: "channelId"
  },
  user: {
    id: "userId"
  },
  actions: [{
    selected_option: {
      value: "selectedOption"
    }
  }]
});

export const getSlackPayloadMock = (p?: Partial<IGenericSlackPayload>): IGenericSlackPayload => ({
  ...getDefaults(),
  ...p
});

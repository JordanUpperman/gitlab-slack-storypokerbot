import { Request, Response } from 'express';
import { WeigherController } from '../../Server/Controllers/Weigher/WeigherController';
import { ISlackWebApi } from '../../Server/ThirdPartyWrappers/Slack/ISlackWebApi';
import { IGitlabInteractor } from '../../Server/ThirdPartyWrappers/Gitlab/IGitlabInteractor';
import { IWeigherController } from '../../Server/Controllers/Weigher/IWeigherController';
import { IConfiguration } from '../../Server/IConfiguration';
import { getConfigurationMock } from '../ConfigurationMock';
import { getSlackPayloadMock } from '../SlackPayloadMock';
import {It, IMock, Mock, Times } from 'typemoq';
import { ChatUpdateArguments, ChatPostMessageArguments, SectionBlock, FilesUploadArguments } from '@slack/web-api';
import { ISlackChat } from 'Server/ThirdPartyWrappers/Slack/ISlackChat';
import { ISlackFiles } from 'Server/ThirdPartyWrappers/Slack/ISlackFiles';
import { ISlackConversations } from 'Server/ThirdPartyWrappers/Slack/ISlackConversations';
import { ISlackInteractor } from '../../Server/ThirdPartyWrappers/Slack/ISlackInteractor';

describe("The WeigherController class", () => {
  
  let weigherController: IWeigherController;
  let slackClientMock:  IMock<ISlackWebApi>;
  let slackChatMock: IMock<ISlackChat>;
  let slackConversationsMock: IMock<ISlackConversations>;
  let slackFilesMock: IMock<ISlackFiles>;
  let gitlabInteractorMock: IMock<IGitlabInteractor>;  
  let slackInteractorMock: IMock<ISlackInteractor>;  
  let configMock: IConfiguration;

  beforeEach(() => {
    gitlabInteractorMock = Mock.ofType<IGitlabInteractor>();
    slackChatMock = Mock.ofType<ISlackChat>();
    slackConversationsMock = Mock.ofType<ISlackConversations>();
    slackFilesMock = Mock.ofType<ISlackFiles>();
    slackClientMock = Mock.ofType<ISlackWebApi>();
    slackInteractorMock = Mock.ofType<ISlackInteractor>();

    slackClientMock.setup(scm => scm.chat).returns(() => slackChatMock.object);
    slackClientMock.setup(scm => scm.conversations).returns(() => slackConversationsMock.object);
    slackClientMock.setup(scm => scm.files).returns(() => slackFilesMock.object);
    slackInteractorMock.setup(sim => sim.client).returns(() => slackClientMock.object);
    configMock = getConfigurationMock({projectsNameAndId: [{name: "project", id: "1"}], userIdsToTagForGrooming: ["userId1", "userId2"]});

    weigherController = new WeigherController(slackInteractorMock.object, gitlabInteractorMock.object, configMock);
  });

  describe("initiateWeiging method", () => {
    beforeEach(() => {
      gitlabInteractorMock.setup(gim => gim.GetIssueData(It.isAnyString(), It.isAnyString()))
      .returns((
        project: string, issuenum: string) => Promise.resolve({
          data: {
            description: `description`, 
            subject: `subject`, 
            web_url: `gitlab/${project}/${issuenum}`, 
            iid: "iid", 
            title: "title"
        }
      }));

      slackChatMock.setup(
        scm => scm.postMessage(It.isAny()))
        .returns((a: ChatPostMessageArguments) => Promise.resolve({ok: true, ts: "expectedTs", channel: a.channel})
      );
    })
    
    const issueUrl = "https://domainisIgnored.this/group/nested-groups-doesntmatter/ProjectsShouldNotMatter/EvenWhenNested/project/-/issues/500";
    
    it("should call GetIssueDetails with the appropriate projectId and IssueNumber from the body.text request", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: ""}} as Request, {send: () => {return;}} as Response, () => {return});
      
      gitlabInteractorMock.verify(gim => gim.GetIssueData("1", "500"), Times.once());
    });

    it("should post message with the given channelId", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: "channel"}} as Request, {send: () => {return;}} as Response, () => {return});

      slackChatMock.verify(scm => scm.postMessage(
        It.is<ChatPostMessageArguments>(o => o.channel == "channel")), 
        Times.once()
      );
    });

    it("should post message with the appropriately formatted message containing the issue link", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: "channel"}} as Request, {send: () => {return;}} as Response, () => {return});

      slackChatMock.verify(scm => scm.postMessage(
        It.is<ChatPostMessageArguments>(o => (o.blocks[0] as SectionBlock).text.text == `<gitlab/1/500|project #iid - title>`)), 
        Times.once()
      );
    });

    it("should post message with appropriately formatted userIds", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: "channel"}} as Request, {send: () => {return;}} as Response, () => {return});

      slackChatMock.verify(scm => scm.postMessage(
        It.is<ChatPostMessageArguments>(o => (o.blocks[2] as SectionBlock).text.text.endsWith("<@userId1>, <@userId2>"))), 
        Times.once()
      );
    });

    it("should upload a file as a reply to the appropriate thread", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: "channel"}} as Request, {send: () => {return;}} as Response, () => {return});

      slackFilesMock.verify(sfm => sfm.upload(
        It.is<FilesUploadArguments>(fua => fua.channels == "channel" && fua.thread_ts === "expectedTs")), 
        Times.once()
      );
    });

    it("should upload a file that contains the issue description", async () => {
      await weigherController.initiateWeiging({body: {text: issueUrl, channel_id: "channel"}} as Request, {send: () => {return;}} as Response, () => {return});

      slackFilesMock.verify(sfm => sfm.upload(
        It.is<FilesUploadArguments>(fua => fua.content === "description")), 
        Times.once()
      );
    })
  });
  
  describe("reloadInitialMessage method", () => {
    it("should update the correct slack message", async () => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      await weigherController.reloadInitialMessage(getSlackPayloadMock({channel: {id: "channelId"}, message: {ts: "messageTs"} as any}));

      slackChatMock.verify(scm => scm.update(
        It.is<ChatUpdateArguments>(cua => cua.ts === "messageTs" && cua.channel === "channelId" )),
        Times.once()
      );
    });

    it("should reset the blocks appropriately", async () => {
      await weigherController.reloadInitialMessage(getSlackPayloadMock({
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        message: {
          blocks: [
            {
              block_id: "users_weighted", 
              type: "section"
            }, {
              block_id: "users_waiting", 
              type: "section", 
              text: {
                text: "waiting for <@userId1>"
              }
            } as SectionBlock
          ]
        } as any
      }));

      slackChatMock.verify(scm => scm.update(
        It.is<ChatUpdateArguments>(cua =>  
          undefined == cua.blocks.find(a => a.block_id == "users_weighted")
          && (cua.blocks.find(b => b.block_id == "users_waiting") as SectionBlock).text.text.indexOf("userId2") > -1)),
        Times.once()
      );    
    });
  });
})
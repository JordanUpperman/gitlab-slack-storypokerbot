import {IConfiguration} from '../Server/IConfiguration';

const getDefaults = (): IConfiguration => ({
  slackApiToken: '',
  slackSigningSecret: '',
  gitlabApiToken: '',
  userIdsToTagForGrooming: [],
  labelsToRemove: [],
  labelsToAdd: [],
  textForGitlabNoteThatContainsLinkToThread: '',
  projectsNameAndId: [],
  initialMessage: '',
  port: 0
});

export const getConfigurationMock = (c?: Partial<IConfiguration>): IConfiguration => ({
  ...getDefaults(),
  ...c
});
import {IConfiguration} from "./IConfiguration";

const getDefaults = (): IConfiguration => ({
  slackApiToken: process.env.NEW_SLACK_TOKEN || '',
  slackSigningSecret: process.env.SLACK_SIGNING_SECRET || '',
  gitlabApiToken: process.env.GITLAB_ACCESST_TOKEN || '',
  userIdsToTagForGrooming: [],
  labelsToRemove: [],
  labelsToAdd: [],
  projectsNameAndId: [],
  textForGitlabNoteThatContainsLinkToThread: "Click here to view the slack thread where this ticket was weighted",    
  initialMessage: "Your vote is needed on a weighing session",
  port: 3000
});

export const getConfiguration = (c?: Partial<IConfiguration>): IConfiguration => ({
  ...getDefaults(),
  ...c
});
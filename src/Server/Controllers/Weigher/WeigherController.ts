import { Block, SectionBlock } from "@slack/web-api";
import { Request, Response } from "express";

import { GenerateIssueBlock } from '../../Blocks/WeigherSpecific/GenerateIssueBlock';
import { GenerateUsersWhoHaveNotRespondedBlock } from '../../Blocks/WeigherSpecific/GenerateUsersWhoHaveNotRespondedBlock'
import { DividerBlock } from '../../Blocks/DividerBlock';
import { GenerateUsersWhoHaveRespondedBlock } from '../../Blocks/WeigherSpecific/GenerateUsersWhoHaveRespondedBlock';
import { FinalizeWeightBlock } from '../../Blocks/WeigherSpecific/FinalizeWeightBlock';
import { GenerateFinalBlock } from '../../Blocks/WeigherSpecific/GenerateFinalBlock';
import { ButtonsRowOne, ButtonsRowTwo } from '../../Blocks/WeigherSpecific/GenerateButtonWeightBlocks';
import { IProjectAndIssueNumber } from '../../ThirdPartyWrappers/Gitlab/IProjectAndIssueNumber';
import { IVotersForWeight } from './IVotersForWeight';
import { IGenericSlackPayload } from '../../ThirdPartyWrappers/Slack/IGenericSlackPayload';
import { IWeigherController } from "./IWeigherController";
import { IGitlabInteractor } from "../../ThirdPartyWrappers/Gitlab/IGitlabInteractor";
import { ISlackChatUpdateResponse } from "../../ThirdPartyWrappers/Slack/Responses/ISlackChatUpdateResponse";
import { IConfiguration } from "../../IConfiguration";
import { ISlackRequestBody } from "../../ThirdPartyWrappers/Slack/ISlackRequestBody";
import { ISlackInteractor } from "../../ThirdPartyWrappers/Slack/ISlackInteractor";

export class WeigherController implements IWeigherController {
    private slackController: ISlackInteractor;
    private gitlabInteractor: IGitlabInteractor;
    private configuration: IConfiguration;

    constructor(slackController: ISlackInteractor, gitlabInteractor: IGitlabInteractor, configuration: IConfiguration) {
        this.slackController = slackController;
        this.gitlabInteractor = gitlabInteractor;
        this.configuration = configuration;
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        this.initializeWeighingSession = this.initializeWeighingSession.bind(this);
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        this.initiateWeiging = this.initiateWeiging.bind(this);
    }

    public async initiateWeiging(request: Request, response: Response): Promise<void> {
        try {
            response.send('');
            const command = request.body as ISlackRequestBody;
            await this.initializeWeighingSession(command.text, command.channel_id);
        } catch (e) {
            console.log(e);
        }
    }
    
    public async reloadInitialMessage(payload: IGenericSlackPayload): Promise<void> {
        try {
            console.log('reloading the initial message');
            await this.slackController.client.chat.update({
                ts: payload.message.ts,
                link_names: true, 
                text: this.configuration.initialMessage, 
                blocks: await Promise.all([...this.resetBlocks(payload.message.blocks)]),
                channel: payload.channel.id 
            });
            console.log('message successfully reloaded');
        } catch (e) {
            console.log(e);
        }
    }
    
    public async updateThreadWithRevealValue(payload: IGenericSlackPayload): Promise<void> {
        try {
            console.log('revealing weights');
            if (payload.message.text === this.configuration.initialMessage) {
                return;
            }
            
            console.log('updating the original message');
            await this.slackController.client.chat.update({
                channel: payload.channel.id, 
                ts: payload.message.ts, 
                text: payload.message.text, 
                blocks: this.removeAllBlocksExcpetIssueLink(payload.message.blocks)
            });

            console.log('posting the message with the weighing votes in the thread');
            await this.slackController.client.chat.postMessage({
                thread_ts: payload.message.thread_ts, 
                channel: payload.channel.id, 
                text: this.convertSecretVotesIntoRevealedVotesMessage(payload.message.text)
            });
            
            console.log('posting block with finalize dropdown');
            await this.slackController.client.chat.postMessage({
                thread_ts: payload.message.thread_ts, 
                channel: payload.channel.id, 
                blocks: [FinalizeWeightBlock], 
                text:""
            });
        } catch (e) {
            console.log(e);
        }
    }

    public async updateSecretFinalWeight(payload: IGenericSlackPayload): Promise<ISlackChatUpdateResponse> {
        try {
            console.log('updating the secret string of finalize block');
            const weightAndUserAsString = `${payload.actions[0].selected_option.value} - ${payload.user.id}`;
            return await this.slackController.client.chat.update({
                channel: payload.channel.id, 
                ts: payload.message.ts, 
                text: weightAndUserAsString,
                blocks: payload.message.blocks
            });
        } catch (e) {
            console.log(e);
        }
    }

    public async updateWeight(weight: string, payload: IGenericSlackPayload): Promise<void> {
        try {
            console.log('update weight with users vote');
            let blocks: Array<Block> = payload.message.blocks;
            const userId = payload.user.id;
            const text = this.addUsersWeight(weight, userId, payload.message.text);
            console.log('updated text: ', text);
            console.log('updating blocks');
            blocks = (blocks).map((b: Block) => {
                if (b.block_id === 'users_waiting') {
                    (b as SectionBlock).text.text = this.removeUserFromWaitingBlock(b as SectionBlock, userId); 
                    return b;
                }
                if (b.block_id === 'users_weighted') {
                    (b as SectionBlock) = GenerateUsersWhoHaveRespondedBlock(this.getUserIdsFromHiddenText(text));
                    return b;
                }
                return b;
            });
            
            this.addUsersWithWeightsBlock(blocks, text);
            
            console.log('updating the message with new values');
            await this.slackController.client.chat.update({
                channel: payload.channel.id, 
                ts: payload.message.ts, 
                text: text, 
                blocks: blocks
            });
        
            console.log('Automatically reveal if all users have weighed');
            if(this.getAllIds((blocks.find(b => b.block_id === 'users_waiting') as SectionBlock).text.text).length === 0){
                console.log('revealing');
                payload.message.text = text;
                await this.updateThreadWithRevealValue(payload)
            }
        } catch (e) {
            console.log(e);
        }
    }
    
    private async initializeWeighingSession(text: string, channelId: string): Promise<void> {
        console.log('starting weighing session');
        const issueDetails = await this.getProjectAndIssueNumberFromIssueLink(text);
        const issue = await this.gitlabInteractor.GetIssueData(issueDetails.projectId, issueDetails.issueNum);
        const issueBlock = GenerateIssueBlock(issue.data, issueDetails.projectName);
        const usersToTag = this.configuration.userIdsToTagForGrooming;
        const usersWeAreWaitingForBlock = GenerateUsersWhoHaveNotRespondedBlock(usersToTag);
        
        console.log('posting the initial message');
        const initialMessage = await this.slackController.client.chat.postMessage({
            link_names: true, 
            text: this.configuration.initialMessage, 
            blocks: [issueBlock, DividerBlock, usersWeAreWaitingForBlock, DividerBlock, ButtonsRowOne, ButtonsRowTwo], 
            channel: channelId
        });

        console.log('uploading the description as a markdown file');
        await this.slackController.client.files.upload({
            thread_ts: initialMessage.ts,
            channels: initialMessage.channel, 
            filetype: "markdown", 
            content: issue.data.description, 
            filename: "description.txt", 
            initial_comment: issue.data.subject
        });
    }

    public async finalizeDecision(payload: IGenericSlackPayload): Promise<ISlackChatUpdateResponse> {
        try {
            console.log('beginning the finalization process');
            
            const userIdAndWeight = (payload.message.text).split(' - ');
            const weight = userIdAndWeight[0];
            const userId = userIdAndWeight[1];
            
            console.log('checking if user associated with secret weight before finalizing process');
            if (userId === payload.user.id) {
                console.log('finalizing');
                await this.AddGitlabNote(weight, payload);
                return await this.slackController.client.chat.update({
                    channel: payload.channel.id, 
                    ts: payload.message.ts, 
                    text: payload.message.text,
                    blocks: [GenerateFinalBlock(`${this.formatUserIdForSlack(userId)} finalized a weight of ${weight}`)]
                });
            }
            console.log('user not associated with secret weight');
        } catch (e) {
            console.log(e);
        }
    }
    
    private resetBlocks(initialBlocks: Array<Block>): Block[] {
        console.log('resetting the blocks to their initial state');
        return initialBlocks.filter(b => b.block_id !== 'users_weighted').map((b) => {
            if (b.block_id === 'users_waiting') {
                const usersToTag = this.configuration.userIdsToTagForGrooming
                return GenerateUsersWhoHaveNotRespondedBlock(usersToTag);
            } else {
                return b;
            }
        });
    }
    private getAllIds(original: string): string[] {
        console.log('getting ids from text');

        const regex = /<@\w*>/;
        const ids: string[] = [];
        while (regex.test(original)) {
            const value = regex.exec(original);
            ids.push(value[0]);
            original = original.substr(value.index + value[0].length);
        }
        console.log('found ids: ', ids);
        return ids;
    }
    
    private formatUserIdForSlack(id: string): string {
        return `<@${id}>`;
    }
    
    private getStringForWeightWithMultipleVotes(voterWeightCombo: IVotersForWeight) {
        const numberOfUsers = voterWeightCombo.voters.length;
        const userIdsFormattedForSlack = voterWeightCombo.voters.map(vwc => this.formatUserIdForSlack(vwc)).join(', ');
        return `*${voterWeightCombo.weight}* - ${numberOfUsers} votes: ${userIdsFormattedForSlack}`;
    }
    
    private getStringForWeightWithSingleVote(voterWeightCombo: IVotersForWeight): string {
        const user = this.formatUserIdForSlack(voterWeightCombo.voters[0]);
        return `*${voterWeightCombo.weight}* - 1 vote: ${user}`;
    }

    private removeUserFromWaitingBlock(block: SectionBlock, userId: string): string {
        console.log('removing users from waiting block');
        let ids = this.getAllIds(block.text.text);
        const user = `<@${userId}>`;
        ids = ids.filter(id => id !== user);
        if (ids) {
            return `Waiting for: ${ids.join(', ')}`; 
        }
        else return ''
    }
    
    private addUsersWeight(weight: string, userId: string, existingText: string): string {
        console.log('adding users weight to the secret string');
        const userWeight = `${userId}-${weight}`;
        let weights = [] as string[];
        if (existingText === this.configuration.initialMessage) {
            return [userWeight].join('|')
        } else {
            weights = existingText.split('|');
            const weightsFromOthers = weights.filter(w => !w.startsWith(userId));
            weights = [...weightsFromOthers, userWeight];
            return weights.join('|')
        }
    }
    
    private getUserIdsFromHiddenText(text: string): string[] {
        console.log('getting users from hidden text');
        return text.split('|').map(t => t.split('-')[0])
    }
    
    private addUsersWithWeightsBlock(blocks: Block[], text: string): void {
        console.log('adding the block of users who have voted');
        if (!blocks.find(b => b.block_id === 'users_weighted')) {
            blocks.pop();
            blocks.pop();
            blocks.push(GenerateUsersWhoHaveRespondedBlock(this.getUserIdsFromHiddenText(text)));
            blocks.push(DividerBlock);
            blocks.push(ButtonsRowOne);
            blocks.push(ButtonsRowTwo);
        }
    }

    private async getProjectAndIssueNumberFromIssueLink(url: string): Promise<IProjectAndIssueNumber> {
        console.log('getting project and issue number from issue link');
        const piecesOfUrl = url.split('/');
        const indexOfDash = piecesOfUrl.indexOf('-');
        const issueNumber = piecesOfUrl[indexOfDash+2];
        const projectName = piecesOfUrl[indexOfDash-1];
        const project = (this.configuration.projectsNameAndId.find(p => p.name === projectName))
        let projectId: string;
        if (project) {
            projectId = project.id
        } else {
            console.log('project was not in the defined project list');
            const matchingProjects = await this.gitlabInteractor.GetProjects(projectName);
            // eslint-disable-next-line 
            projectId = (matchingProjects.data.find(p => p.path === projectName)).id.toString();
        }
        return {projectId: projectId, issueNum: issueNumber, projectName: projectName}
    }

    private async getIssueUrlFromOriginalMessageInThread(payload: IGenericSlackPayload): Promise<string> {
        console.log('getting the issue url from the original post');
        const messageThread = await this.slackController.client.conversations.history({channel: payload.channel.id, latest: payload.message.ts})
        const textFromOriginalMessage = messageThread.messages.find(m => m.ts === payload.message.thread_ts).blocks[0].text.text;
        return textFromOriginalMessage.substr(0, textFromOriginalMessage.indexOf('|'));
    }

    private async AddGitlabNote(weight: string, payload: IGenericSlackPayload) {
        console.log('constructing and executing gitlab interaction');
        const permalink = await this.slackController.client.chat.getPermalink({message_ts: payload.message.ts, channel: payload.channel.id});
        const issueUrl = await this.getIssueUrlFromOriginalMessageInThread(payload);
        const issueAndProject = await this.getProjectAndIssueNumberFromIssueLink(issueUrl);
        const note = `[${this.configuration.textForGitlabNoteThatContainsLinkToThread}](${permalink.permalink})`;
        await this.gitlabInteractor.AddNote(issueAndProject.projectId, issueAndProject.issueNum, note);
        await this.gitlabInteractor.EditIssue(issueAndProject.projectId, issueAndProject.issueNum, this.configuration.labelsToAdd, this.configuration.labelsToRemove, weight);
    }

    private getArrayOfVoterAndWeightCombos(messageText: string): Array<Array<string | number>> {
      console.log('splitting string of voters into arrays of id and weight combos');
      return (messageText  // userId - weight | userid - weight | ...
        .split('|')
        .map(t => t.split('-')) as Array<Array<string | number>>)
        .sort((a: Array<number | any>, b: Array<number | any>) => a[1] - b[1]);
    }

    private groupVotersWhoVotedForTheSameWeight(voterAndWeightCombos: Array<Array<string | number>>): IVotersForWeight[] {
      console.log('grouping voters into like weights and sorting them');
      const votersAndWeights: Array<IVotersForWeight> = [];
      voterAndWeightCombos.map(vaw => {
        const itemWithMatchingWeight = votersAndWeights.find(v => v.weight === vaw[1]);
        if (itemWithMatchingWeight) {
          votersAndWeights[votersAndWeights.indexOf(itemWithMatchingWeight)].voters.push(vaw[0] as string);
        } else {
          votersAndWeights.push({voters: [vaw[0] as string], weight: vaw[1]})
        }
      });

      return votersAndWeights;
    }


    private removeAllBlocksExcpetIssueLink(blocks: Block[]): Block[] {
      console.log('remove blocks other than the issue link');
      return blocks.filter(b => 
        b.block_id !== 'users_waiting' &&
        b.block_id !== 'users_weighted' &&
        b.type !== 'actions' &&
        b.type !== 'divider'
      );
    }

    private convertSecretVotesIntoRevealedVotesMessage(secretVotes: string): string {
      console.log('converting voter string to slack message');
      const voterAndWeightCombos = this.getArrayOfVoterAndWeightCombos(secretVotes)
      const group: Array<IVotersForWeight> = this.groupVotersWhoVotedForTheSameWeight(voterAndWeightCombos);
      
      const reversedArrayOfFormattedStrings = group.map(userVoteCombo => {
        if (userVoteCombo.voters.length === 1) {
          return this.getStringForWeightWithSingleVote(userVoteCombo)
        } else {
          return this.getStringForWeightWithMultipleVotes(userVoteCombo);
        }
      });
      
      return reversedArrayOfFormattedStrings.reverse().join('\n\n');
    }
}
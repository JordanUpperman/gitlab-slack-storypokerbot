export interface IVotersForWeight {
  voters: Array<string>,
  weight: string | number;
}

import { IGenericSlackPayload } from "../../ThirdPartyWrappers/Slack/IGenericSlackPayload";
import { ISlackChatUpdateResponse } from "../../ThirdPartyWrappers/Slack/Responses/ISlackChatUpdateResponse";

export interface IWeigherController {
  initiateWeiging(request, response, next); 
  reloadInitialMessage(payload: IGenericSlackPayload): Promise<void>;
  updateThreadWithRevealValue(payload: IGenericSlackPayload): Promise<void>;
  updateSecretFinalWeight(payload: IGenericSlackPayload): Promise<ISlackChatUpdateResponse>;
  updateWeight(weight: string, payload: IGenericSlackPayload): Promise<void>;
  finalizeDecision(payload: IGenericSlackPayload): Promise<ISlackChatUpdateResponse>;
}
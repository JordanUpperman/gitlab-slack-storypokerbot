export interface IConfiguration {
  slackApiToken: string;
  slackSigningSecret: string;
  gitlabApiToken: string;
  userIdsToTagForGrooming: Array<string>,
  labelsToRemove: Array<string>,
  labelsToAdd: Array<string>,
  projectsNameAndId: Array<{name: string, id: string}>
  textForGitlabNoteThatContainsLinkToThread: string;
  initialMessage: string;
  port: number
}
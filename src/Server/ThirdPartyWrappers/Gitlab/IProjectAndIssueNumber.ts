export interface IProjectAndIssueNumber {
  projectId: string, 
  issueNum: string,
  projectName: string
}

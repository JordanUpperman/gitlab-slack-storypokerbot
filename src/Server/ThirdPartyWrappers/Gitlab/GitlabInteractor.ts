import { IGitlabInteractor } from './IGitlabInteractor';
import { IGitlabAddNoteResponseData } from "./Responses/IGitlabAddNoteResponseData";
import { IGitlabIssueResponseData } from "./Responses/IGitlabIssueResponseData";
import { IHttpHandler } from '../Axios/IHttpHandler';
import { IHttpResponse } from '../Axios/IHttpResponse';
import { IGitlabProjectResponseData } from './Responses/IGitlabProjectResponseData';

export class GitlabInteractor implements IGitlabInteractor {
    private token: string;
    private httpHandler: IHttpHandler;

    constructor(httpHandler: IHttpHandler, token: string) {
      this.httpHandler = httpHandler;
      this.token = token;
    }

    public async GetIssueData(projectId: string, issueNum: string): Promise<IHttpResponse<IGitlabIssueResponseData>> {
      return await this.httpHandler.get(
        `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueNum}`,
        {
          headers: { "PRIVATE-TOKEN": this.token }
        });
    }


    public async AddNote(projectId: string, issueNum: string, body: string): Promise<IHttpResponse<IGitlabAddNoteResponseData>> {
      return await this.httpHandler.post(
        `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueNum}/notes`,
        { body: body },
        {
          headers: { "PRIVATE-TOKEN": this.token }
        });
    }


    public async EditIssue(projectId: string, issueNum: string, labelsToAdd: string[], labelsToRemove: string[], weight: string): Promise<IHttpResponse<IGitlabIssueResponseData>> {
      return await this.httpHandler.put(
        `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueNum}`,
        {
          add_labels: labelsToAdd.join(","),
          remove_labels: labelsToRemove.join(","),
          weight: weight
        },
        {
          headers: { "PRIVATE-TOKEN": this.token }
        });
    }

    public async GetProjects(projectName: string): Promise<IHttpResponse<Array<IGitlabProjectResponseData>>> {
      return await this.httpHandler.get(
        `https://gitlab.com/api/v4/projects/?simple=true&membership=true&search=${projectName}`,
        {
          headers: { "PRIVATE-TOKEN": this.token }
        });
    }
}

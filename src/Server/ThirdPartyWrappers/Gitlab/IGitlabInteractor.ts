import { IHttpResponse } from "../Axios/IHttpResponse";
import { IGitlabIssueResponseData } from "./Responses/IGitlabIssueResponseData";
import { IGitlabEditIssueResponseData } from "./Responses/IGitlabEditIssueResponseData";
import { IGitlabAddNoteResponseData } from "./Responses/IGitlabAddNoteResponseData";
import { IGitlabProjectResponseData } from "./Responses/IGitlabProjectResponseData";

export interface IGitlabInteractor {
  GetIssueData(projectId: string, issueNum: string): Promise<IHttpResponse<IGitlabIssueResponseData>>;
  AddNote(projectId: string, issueNum: string, body: string): Promise<IHttpResponse<IGitlabAddNoteResponseData>>;
  EditIssue(projectId: string, issueNum: string, labelsToAdd: string[], labelsToRemove: string[], weight: string): Promise<IHttpResponse<IGitlabEditIssueResponseData>>;
  GetProjects(projectName: string): Promise<IHttpResponse<Array<IGitlabProjectResponseData>>>
}
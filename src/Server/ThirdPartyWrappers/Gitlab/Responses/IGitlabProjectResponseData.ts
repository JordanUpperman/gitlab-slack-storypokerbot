export interface IGitlabProjectResponseData {
    id: number;
    path: string;
}
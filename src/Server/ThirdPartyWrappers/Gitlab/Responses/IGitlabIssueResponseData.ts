export interface IGitlabIssueResponseData {
  description: string;
  subject: string;
  web_url: string;
  iid: string;
  title: string;
}

import { ISlackResponseBase } from "./ISlackResponseBase";

export interface ISlackChatGetPermaLinkResponse extends ISlackResponseBase {
  permalink?: string;
}

import { ISlackResponseBase } from "./ISlackResponseBase";

export type ISlackChatUpdateResponse = ISlackResponseBase

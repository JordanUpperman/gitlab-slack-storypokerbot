import { ISlackResponseBase } from "./ISlackResponseBase";

export type ISlackFilesUploadResponse = ISlackResponseBase

import { ISlackResponseBase } from "./ISlackResponseBase";

export interface ISlackChatPostMessageResponse extends ISlackResponseBase {
  ts?: string;
  channel?: string;
}

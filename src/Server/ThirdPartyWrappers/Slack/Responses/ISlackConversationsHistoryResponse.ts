import { ISlackResponseBase } from "./ISlackResponseBase";

export interface ISlackConversationsHistoryResponse extends ISlackResponseBase {
  messages?: Array<{
    ts: string
    blocks: Array<{
      text: {
        text: string;
      };
    }>;
  }>;
}

import { ChatUpdateArguments, ChatPostMessageArguments, ChatGetPermalinkArguments } from "@slack/web-api";
import { ISlackChatUpdateResponse } from "./Responses/ISlackChatUpdateResponse";
import { ISlackChatPostMessageResponse } from "./Responses/ISlackChatPostMessageResponse";
import { ISlackChatGetPermaLinkResponse } from "./Responses/ISlackChatGetPermaLinkResponse";

export interface ISlackChat {
  getPermalink(cgpa: ChatGetPermalinkArguments): Promise<ISlackChatGetPermaLinkResponse>;
  postMessage(args: ChatPostMessageArguments): Promise<ISlackChatPostMessageResponse>;
  update(args: ChatUpdateArguments): Promise<ISlackChatUpdateResponse>;
}
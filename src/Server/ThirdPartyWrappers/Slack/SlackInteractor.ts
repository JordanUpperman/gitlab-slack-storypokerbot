import { IConfiguration } from "Server/IConfiguration";
import { createMessageAdapter } from "@slack/interactive-messages";
import SlackMessageAdapter from "@slack/interactive-messages/dist/adapter";
import { WebClient } from "@slack/web-api";
import { ISlackWebApi } from "Server/ThirdPartyWrappers/Slack/ISlackWebApi";
import { ISlackInteractor } from "./ISlackInteractor";
import { Request, Response, NextFunction } from "express";

export class SlackInteractor implements ISlackInteractor {
    public actions: SlackMessageAdapter;
    public client: ISlackWebApi;

    constructor(configuration: IConfiguration) {
        this.actions = createMessageAdapter(configuration.slackSigningSecret);
        this.client = new WebClient(configuration.slackApiToken);
    }
}
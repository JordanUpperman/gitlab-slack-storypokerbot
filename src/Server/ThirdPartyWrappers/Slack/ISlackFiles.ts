import { FilesUploadArguments } from "@slack/web-api";
import { ISlackFilesUploadResponse } from "./Responses/ISlackFilesUploadResponse";

export interface ISlackFiles {
  upload(args: FilesUploadArguments): Promise<ISlackFilesUploadResponse>;
}

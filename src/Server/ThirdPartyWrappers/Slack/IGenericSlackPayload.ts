import { Block } from "@slack/types";

export interface IGenericSlackPayload {
  message: {
    text: string
    ts: string,
    blocks: Array<Block>,
    thread_ts: string
  },
  channel: {
    id: string
  },
  user: {
    id: string
  },
  actions: Array<{
    selected_option: {
      value: string
    }
  }>
}

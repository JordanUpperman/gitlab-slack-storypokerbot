import { ConversationsHistoryArguments } from "@slack/web-api";
import { ISlackConversationsHistoryResponse } from "./Responses/ISlackConversationsHistoryResponse";

export interface ISlackConversations {
  history(args: ConversationsHistoryArguments): Promise<ISlackConversationsHistoryResponse>;
}

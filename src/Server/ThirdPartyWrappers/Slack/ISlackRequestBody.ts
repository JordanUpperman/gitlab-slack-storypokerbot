export interface ISlackRequestBody {
  text: string,
  channel_id:string
}
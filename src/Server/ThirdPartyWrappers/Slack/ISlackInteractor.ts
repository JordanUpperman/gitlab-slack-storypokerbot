import SlackMessageAdapter from "@slack/interactive-messages/dist/adapter";
import { ISlackWebApi } from "Server/ThirdPartyWrappers/Slack/ISlackWebApi";

export interface ISlackInteractor {
    actions: SlackMessageAdapter;
    client: ISlackWebApi;
}

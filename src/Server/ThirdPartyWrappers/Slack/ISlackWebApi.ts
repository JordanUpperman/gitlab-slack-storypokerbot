import { ISlackChat } from "./ISlackChat";
import { ISlackConversations } from "./ISlackConversations";
import { ISlackFiles } from "./ISlackFiles";

export interface ISlackWebApi {
  token?: string;
  chat: ISlackChat;
  files: ISlackFiles;
  conversations: ISlackConversations;
}
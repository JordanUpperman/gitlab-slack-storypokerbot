interface IHeaders {
  "PRIVATE-TOKEN"?: string
}

export interface IHttpConfiguration {
  headers: IHeaders;
}

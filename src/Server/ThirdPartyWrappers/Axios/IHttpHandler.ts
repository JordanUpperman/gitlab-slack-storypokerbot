import { IHttpResponse } from "./IHttpResponse";
import { IHttpConfiguration } from "./IHttpConfiguration";

export interface IHttpHandler {
  get<T>(url: string, configuration: IHttpConfiguration): Promise<IHttpResponse<T>>
  post<T,TBody>(url: string, body: TBody, configuration: IHttpConfiguration): Promise<IHttpResponse<T>>
  put<T,TBody>(url: string, body: TBody, configuration: IHttpConfiguration): Promise<IHttpResponse<T>>
}

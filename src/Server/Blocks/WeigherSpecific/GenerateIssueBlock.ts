import { SectionBlock } from "@slack/types";
import { IGitlabIssueResponseData } from "Server/ThirdPartyWrappers/Gitlab/Responses/IGitlabIssueResponseData";

export function GenerateIssueBlock(issueData: IGitlabIssueResponseData, project: string): SectionBlock  {
  return {
    "type": "section",
    "block_id": "issue",
    "text": {
      "type": "mrkdwn",
      "text": `<${issueData.web_url}|${project} #${issueData.iid} - ${issueData.title}>`
    }
  }
}

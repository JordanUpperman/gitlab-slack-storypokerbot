import { SectionBlock } from "@slack/types";

export function GenerateUsersWhoHaveRespondedBlock(usersToTag: string[]): SectionBlock { 
  return {
    "type": "section",
    "block_id": "users_weighted",
    "text": {
      "type": "mrkdwn",
      "text": `Received Weight From: <@${usersToTag.join('>, <@')}>`
    }
  }
}
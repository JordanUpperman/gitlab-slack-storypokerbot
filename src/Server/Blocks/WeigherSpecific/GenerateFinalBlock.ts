import { SectionBlock } from "@slack/types";

export function GenerateFinalBlock(message: string): SectionBlock {
  return {
    "type": "section",
    "block_id": "final",
    "text": {
      "type": "mrkdwn",
      "text": message
    }
  }
}
import { Button, ActionsBlock } from "@slack/types"

function weightButton(weight: string): Button {
  return {
    "type": "button",
    "text": {
      "type": "plain_text",
      "text": weight,
      "emoji": true
    },
    "value": weight,
    "action_id": weight
  }
}

const ButtonsRowOne: ActionsBlock = {
  "type": "actions",
  "elements": [
    weightButton('0'),
    weightButton('1'),
    weightButton('2'),
    weightButton('3'),
    weightButton('5')
  ]
}

const ButtonsRowTwo: ActionsBlock = {
    "type": "actions",
    "elements": [
    weightButton('8'),
    weightButton('13'),
    weightButton(':scream:'),
        {
            "type": "button",
            "style": "danger",
            "text": {
                "type": "plain_text",
                "text": "Re-weigh",
                "emoji": true
            },
            "value": "reweigh",
            "action_id": "reweigh"
        },
        {
            "type": "button",
            "style": "danger",
            "text": {
                "type": "plain_text",
                "text": "Reveal",
                "emoji": true
            },
            "value": "reveal",
            "action_id": "reveal"
        }
    ]
}

export { ButtonsRowOne, ButtonsRowTwo }
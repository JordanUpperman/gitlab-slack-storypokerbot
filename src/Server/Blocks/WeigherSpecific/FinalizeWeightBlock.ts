import { ActionsBlock, Option } from "@slack/types"

function dropdownOption(value: string): Option {
  return {
    "text": {
      "type": "plain_text",
      "text": value,
      "emoji": true
    },
    "value": value
  }
}

const FinalizeWeightBlock: ActionsBlock = {
  "type": "actions",
  "elements": [
    {
      "action_id": "Final_Weight",
      "type": "static_select",
      "placeholder": {
        "type": "plain_text",
        "text": "Select an item",
        "emoji": true
      },
      "options": [
        dropdownOption("0"),
        dropdownOption("1"),
        dropdownOption("2"),
        dropdownOption("3"),
        dropdownOption("5"),
        dropdownOption("8")
      ]
    },
    {
      "type": "button",
      "style": "primary",
      "text": {
        "type": "plain_text",
        "text": "Finalize",
        "emoji": true
      },
      "value": "finalize",
      "action_id": "finalize"
    }
  ]
}

export { FinalizeWeightBlock }

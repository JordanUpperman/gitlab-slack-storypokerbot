import { SectionBlock } from "@slack/types";

export function GenerateUsersWhoHaveNotRespondedBlock(usersToTag: string[]): SectionBlock { 
  return {
    "type": "section",
    "block_id": "users_waiting",
    "text": {
      "type": "mrkdwn",
      "text": `Waiting for: <@${usersToTag.join('>, <@')}>`
    }
  }
}

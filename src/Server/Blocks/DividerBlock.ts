import { DividerBlock } from "@slack/types";

const DividerBlock: DividerBlock = {
  "type": "divider"
}

export { DividerBlock }

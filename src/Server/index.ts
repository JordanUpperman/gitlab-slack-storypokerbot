import { IAppConfig } from './app';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import { WeigherRoutes } from './Routes/WeigherRoutes';
import { getConfiguration } from './configuration';
import { WeigherController } from './Controllers/Weigher/WeigherController';
import { GitlabInteractor } from './ThirdPartyWrappers/Gitlab/GitlabInteractor';
import * as axios from 'axios';
import { SlackInteractor } from './ThirdPartyWrappers/Slack/SlackInteractor';

function generateApp(): IAppConfig {
  const config = getConfiguration();
  const slackController = new SlackInteractor(config);
  const gitlabInteractor = new GitlabInteractor(axios.default, config.gitlabApiToken);
  const weigherController = new WeigherController(slackController, gitlabInteractor, config);

  return {
    port: config.port,
    routers: [
      new WeigherRoutes(slackController, weigherController)
    ],
    middleWares: [
      morgan('tiny'),
      bodyParser.urlencoded({extended: true}),
      bodyParser.json()
    ],
    slackInteractor: slackController
  }
}
export {generateApp};
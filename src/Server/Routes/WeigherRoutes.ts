import * as express from 'express';
import { IRouterBase } from './IRouterBase';
import { IWeigherController } from 'Server/Controllers/Weigher/IWeigherController';
import { ISlackInteractor } from 'Server/ThirdPartyWrappers/Slack/ISlackInteractor';

export class WeigherRoutes implements IRouterBase {
  private slack: ISlackInteractor;
  private weigherController: IWeigherController;
  public router = express.Router();
  
  constructor(slack: ISlackInteractor, slackWeigherController: IWeigherController) {
    this.slack = slack;
    this.weigherController = slackWeigherController;
    this.initRoutes();
  }

  public initRoutes(): void {
    this.router.use('/gitlab/weigh', this.weigherController.initiateWeiging.bind(this.weigherController));

    this.slack.actions.action({actionId: "0"}, async (payload) => {
      return await this.weigherController.updateWeight("0", payload);
    });
    
    this.slack.actions.action({actionId: "1"}, async (payload) => {
      return await this.weigherController.updateWeight("1", payload);
    });
    
    this.slack.actions.action({actionId: "2"}, async (payload) => {
      return await this.weigherController.updateWeight("2", payload);
    });
    
    this.slack.actions.action({actionId: "3"}, async (payload) => {
      return await this.weigherController.updateWeight("3", payload);
    });
    
    this.slack.actions.action({actionId: "5"}, async (payload) => {
      return await this.weigherController.updateWeight("5", payload);
    });
    
    this.slack.actions.action({actionId: "8"}, async (payload) => {
      return await this.weigherController.updateWeight("8", payload);
    });
    
    this.slack.actions.action({actionId: "13"}, async (payload) => {
      return await this.weigherController.updateWeight("13", payload);
    });
    
    this.slack.actions.action({actionId: ":scream:"}, async (payload) => {
      return await this.weigherController.updateWeight(":scream:", payload);
    });
    
    this.slack.actions.action({actionId: "reweigh"}, async (payload) => {
      await this.weigherController.reloadInitialMessage(payload);
    });
    
    this.slack.actions.action({actionId: "reveal"}, async (payload) => {
      await this.weigherController.updateThreadWithRevealValue(payload);
    });
    
    this.slack.actions.action({actionId: "finalize"}, async (payload) => {
      return await this.weigherController.finalizeDecision(payload);
    });
    
    this.slack.actions.action({actionId: "Final_Weight"}, async (payload) => {
      return await this.weigherController.updateSecretFinalWeight(payload);
    });
  }
}

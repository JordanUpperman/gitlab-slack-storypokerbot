import { Router } from "express";

export interface IRouterBase {
  initRoutes(): void
  router: Router;
}

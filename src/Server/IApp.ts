import * as express from 'express';

export interface IApp {
  app: express.Application;
  port: number;
  router: express.Router;
  listen(): void 
}

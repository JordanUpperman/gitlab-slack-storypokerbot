import * as express from 'express'
import { Router } from 'express'
import { IRouterBase } from './Routes/IRouterBase';
import { IApp } from './IApp';
import { ISlackInteractor } from './ThirdPartyWrappers/Slack/ISlackInteractor';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type middleWares = { forEach: (arg0: (middleWare: any) => void) => void; };
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type routes = { forEach: (arg0: (router: any) => void) => void; };

export type IAppConfig = { 
  port: number; 
  middleWares: 
  middleWares; 
  routers: routes; 
  slackInteractor: ISlackInteractor
};

class App implements IApp {
  public app: express.Application;
  public port: number;
  public router: express.Router;

  constructor(app: IAppConfig) {
    console.log('creating app');
    this.app = express();
    this.router = Router();
    this.port = app.port;
    this.app.use('/slack/actions', app.slackInteractor.actions.expressMiddleware());
    this.middlewares(app.middleWares);
    this.routes(app.routers);
  }

  private middlewares(middleWares: middleWares) {
    middleWares.forEach(middleWare => {
      this.app.use(middleWare);
    });
  }

  private routes(routes: routes) {
    routes.forEach((router: IRouterBase) => {
      this.app.use('/', router.router);
    });
  }

  public listen(): void {
    this.app.listen(this.port, () => {
      console.log(`App listening on the http://localhost:${this.port}`)
    });
  }
}

export default App